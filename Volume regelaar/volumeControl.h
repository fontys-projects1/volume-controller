/*
 Name:		volumeControl.h
 Created:	13-10-2022 17:13:51 AM
 Author:	Tommy de Wever
*/

#pragma once
#include <Arduino.h>

const int channelMaxValue = 100;		// value for the maximum value
const int channelMinValue = 5;			// value for the minimum value


enum Channel
{
	left,
	right,
	both
};

enum MuteState
{
	normal,
	goToNormal,
	goToMinimal,
	minimal
};

// add a value to the volume of a specific channel, accepts also both channels
// needs the address of the volume storage, the channel and the value to add
void editVolumeMemory(uint8_t* volume, enum Channel channel, int8_t value);

// this function needs to be called constantly
// if the mute state cheages, this function will handel the volume levels (fading in and out)
// needs the address of the volume storage and the address of the muteState 
void mute_processor(uint8_t* volume, uint8_t* muteState);
