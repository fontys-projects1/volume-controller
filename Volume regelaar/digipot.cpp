/*
 Name:		digipot.cpp
 Created:	11-10-2022 17:25:24 AM
 Author:	Tommy de Wever
*/

#include "digipot.h"

// consturctor, defines the pins and set them to default
// needs the chip select pin, the increment pin and the up/down pin
digipot::digipot(uint8_t chipSelect, uint8_t increment, uint8_t upDown)
{
	// store the pin numbers to the memory
	pin_chipSelect = chipSelect;
	pin_increment = increment;
	pin_upDown = upDown;

	// set the pinmode
	pinMode(pin_chipSelect, OUTPUT);
	pinMode(pin_increment, OUTPUT);
	pinMode(pin_upDown, OUTPUT);

	// set the outputs to there default position
	digitalWrite(pin_chipSelect, HIGH);
	digitalWrite(pin_increment, LOW);
	digitalWrite(pin_upDown, LOW);
}

// go to zero, resets the valueMemory
void digipot::goToZero(void)
{
	writePot(100, false);
	valueMemory = 0;
}

void digipot::writePot(uint8_t steps, bool goingUp)
{
	// enable the potmeter (chipselect) to comunicate to the potmeter
	digitalWrite(pin_chipSelect, LOW);

	// wait to activate
	delayMicroseconds(50);

	// volume up or down
	digitalWrite(pin_upDown, goingUp);

	// wait to activate
	delayMicroseconds(1);

	// write the steps
	for (uint8_t i = 0; i < steps; i++)
	{
		digitalWrite(pin_increment, LOW);
		delayMicroseconds(1);
		digitalWrite(pin_increment, HIGH);
		delayMicroseconds(1);
	}

	// set the ouputs to default

	// up/down low
	digitalWrite(pin_upDown, LOW);

	// disable the potentiometer (chipselect)
	digitalWrite(pin_chipSelect, HIGH);

	// wait to activate
	delayMicroseconds(1);
}

// check if the new value is in range, else adjust to the limits
// store the new value to the memory and set the potentoometer
void digipot::setPot(uint8_t value)
{	uint8_t newValue = value;

	// check if the value is in range
	// check if the value is smaller the the minimal value
	if (newValue < minValue)
	{
		// set to the minimal value
		newValue = minValue;
	}
	else if (newValue > maxValue)
		// check if the value is greater than the maximum value
	{
		// set to the maximum value
		newValue = maxValue;
	}

	// calculate the diffrece
	uint8_t steps = abs(valueMemory - newValue);

	// check if the value is already set, is the newValue really new?
	if (valueMemory != newValue)
	{
		// write potentiometer
		writePot(steps, (newValue > valueMemory));	// (newValue > valueMemory) defines the up or down direction
		// save the new potentiometer value to the memory
		valueMemory = newValue;
	}
}