/*
 Name:		pushButton.cpp
 Created:	11-10-2022 17:27:24 AM
 Author:	Tommy de Wever
*/


#include "pushButton.h"

// predefines
const int debouceDelay = 20;			// time of debounce delay [ms]
const int holdingDownTime = 600;		// time to define the button is holding down [ms]

// constructor
// defines the pinmode
pushButton::pushButton(uint8_t pin)
{
	m_pin = pin;
	pinMode(m_pin, INPUT);
}

// constructor for the interval
pushButton::interval_activator::interval_activator() {};

// the activate function needs a time in ms and will return true when the given time exeeds
// this function will reset te timer automaticly
bool pushButton::interval_activator::activate(uint16_t time)
{
	bool returnValue = false;
	// loop with a interval of the givven time
	if (millis() - lastUpdate > time)
	{
		// reset the timer
		lastUpdate = millis();

		// return ture
		returnValue = true;
	}
	return returnValue;
}

// private function
// get the push state, pushed or holding doen
// call only after debouce
void pushButton::executeFunctions(uint16_t repeatWithDelay, bool* pressedState)
{   
	// only run when the button is pressed
	if (state)
	{
		// regist that te button has pressed
		if (!hasPressed)
		{
			hasPressed = true;
		}

		// check if the button is holding down
		if (millis() - timeLastChange > holdingDownTime)
		{
			// regist that th button was holding down
			if (!isHoldingDown)
			{
				isHoldingDown = true;
			}

			// when a repeat delay is given
			if (repeatWithDelay > 0)
			{
				// loop with a interval of the givven time
				if (intern_interval.activate(repeatWithDelay))
				{
					// call the function holding down
					if (functionHoldingDown != nullptr)	functionHoldingDown();
				}
			}
			else 
				// call the holdingdown function with no delay
			{
				// call the function holding down
				if (functionHoldingDown != nullptr) functionHoldingDown();
			}
		}
	}
	else 
		// when the button is released
	{
		// the button has pressed
		if (hasPressed)
		{
			// when the button was holding down
			if (isHoldingDown)
			{
				// do nothing else but reset 
				isHoldingDown = false;
			}
			else if (millis() - timeLastChange < holdingDownTime)
				// was not holding down
			{
				*pressedState = true;
				// call the function has pressed
				if (functionPressed != nullptr) functionPressed();
			}
			// reset this button function
			hasPressed = false;
		}
	}
}

// read the button status with debounce, store the reading in memory
// to read the status in memory, call getSTate()
bool pushButton::read(func pressedFunc = nullptr, func holdingDownFunc = nullptr, uint16_t repeatWithDelay = 0)
{
	bool pressedState = false;

	// read the (inverted) input
	bool input = !digitalRead(m_pin);

	// store the callback functions
	functionPressed = pressedFunc;
	functionHoldingDown = holdingDownFunc;

	// save the time of the last button change
	if (input != lastState)
	{
		timeLastChange = millis();
	}

	// only continue when the last change is longer then the debouce delay
	if ((millis() - timeLastChange) > debouceDelay)
	{
		// if the button state has changed, toggle button state
		if (input != state)
		{
			state = input;
		}

		// execute the correct function
		executeFunctions(repeatWithDelay, &pressedState);
	}

	// store this state to compare in the next call
	lastState = input;

	// return the current pressed state
	return pressedState;
}
