/*
 Name:		pushButton.h
 Created:	11-10-2022 17:27:24 AM
 Author:	Tommy de Wever
*/


#pragma once
#include <Arduino.h>

class pushButton
{
	// add a inteval class as a delay function for loops
	class interval_activator
	{
	private:
		// store the last update time in privet
		uint32_t lastUpdate;
	public:
		// constroctor with nothing
		interval_activator(void);		

		// this function needs a time in ms and returns true if the time the current
		// time + the given time exeeds 
		// this functon will reset automaticly and will return true after the fiven time
		bool activate(uint16_t time);	
	};

private:
	uint8_t m_pin;				// the button pin		
	bool hasPressed = false;	// helper for internal function
	bool lastState = false;		// helper for internal function

	// create the interval function for internal use
	interval_activator intern_interval;

	// create the structure of the functions what will be stored
	typedef void (*func)(void);

	// store the external functions
	func functionPressed;		// store the function, will be called when the button has pressed (and was not holding down)
	func functionHoldingDown;	// store the function, will be called when the button is holding down

	// execute the pressed function or the holding down function
	// the holding down function will bee called without a delay,
	// if a (ms) value is given, the function will be called with a interval of the given value
	void executeFunctions(uint16_t repeatWithDelay, bool* pressedState);

public:
	uint32_t timeLastChange;		// last time the button changeds state
	bool state;						// the current button state with debouce applyed
	bool isHoldingDown = false;		// state if the button is holding down

	interval_activator interval;

	// init the pushbutton
	// constructor, add input pin
	// defines the pinmode
	pushButton(uint8_t pin);

	// read button and activate function(s)
	// if button is pressed, call function pressedFunc
	// if button is holding donw, call function holdingDownFunc (optional)
	// holdingDownFunc can be repeated in a intervan of repeatWithDelay (optional) (0 = no delay) [time in ms]
	// read(pressedFunc [normaly nullptr], holdingDOwnFunc [normaly nullptr], repeastWithDelay [normaly 0]
	bool read(func pressedFunc = nullptr, func holdingDownFunc = nullptr, uint16_t repeatWithDelay = 0);
};

