/*
 Name:		digipot.h
 Created:	11-10-2022 17:25:24 AM
 Author:	Tommy de Wever
*/


#pragma once
#include <Arduino.h>
class digipot
{
private:
	// store the needed pins
	uint8_t pin_chipSelect;
	uint8_t pin_increment;
	uint8_t pin_upDown;

	// the final function to write the the potentiometer
	void writePot(uint8_t steps, bool goingUp);

public:
	// constructor
	// define the chip select pin, the increment pin and the up/down pin
	digipot(uint8_t chipSelect, uint8_t increment, uint8_t upDown);

	// go to zero, reset valueMemory
	void goToZero(void);

	// the value storage
	uint8_t valueMemory;

	// set the potentiometer to the fiven value
	// the value needs to been bigger or equal to minValue and smaller or equal to maxValue
	void setPot(uint8_t value);

	uint8_t maxValue = 100;		// value for the maximum value
	uint8_t minValue = 0;		// value for the minimum value
};

