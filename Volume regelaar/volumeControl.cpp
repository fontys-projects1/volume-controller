/*
 Name:		volumeControl.cpp
 Created:	13-10-2022 17:13:51 AM
 Author:	Tommy de Wever
*/


#include "volumeControl.h"

// defines the fade in/out time
const int goToMuteTime = 400;			// total time in ms to go to the mute mode
const int goToNormalTime = 1000;		// total time in ms to go to the normal mode

// add a value to the current volume.
// value address, channel(s) to apply, the value to apply
void editVolumeMemory(uint8_t* volume, enum Channel channel, int8_t value)
{
	if (channel == Channel::both)
	{
		// start the same function for a single channel
		editVolumeMemory(volume, Channel::left, value);
		editVolumeMemory(volume, Channel::right, value);
	}
	else
	{
		// the new value is to big
		if (volume[channel] + value >= channelMaxValue)
		{
			volume[channel] = channelMaxValue;
		}
		// the new value is to small
		else if (volume[channel] + value <= channelMinValue)
		{
			volume[channel] = channelMinValue;
		}
		// the value is legal
		else
		{
			volume[channel] += value;
		}
	}
}

// handels fading in/out 
void mute_processor(uint8_t* volume, uint8_t* muteState)
{
	static int8_t oldVolume[2];
	static uint8_t counter;
	static uint32_t lastUpdateTime;
	static uint8_t maxValue;

	switch (*muteState)
	{
	case MuteState::normal:
		// store the currecnt value to the the storage of this function
		oldVolume[Channel::left] = volume[Channel::left];
		oldVolume[Channel::right] = volume[Channel::right];
		break;

	case MuteState::goToNormal:
		// going to the old volume

		// select the highest volume
		maxValue = (oldVolume[Channel::left] > oldVolume[Channel::right]) ? oldVolume[Channel::left] : oldVolume[Channel::right];

		// only activate in a interval
		// the interval is the time this fuction needs to be, devided by the maximaum value
		if (millis() - lastUpdateTime > goToNormalTime / maxValue)
		{
			lastUpdateTime = millis();

			// when the current volume is set to the old volume
			if (volume[Channel::left] >= oldVolume[Channel::left] && volume[Channel::right] >= oldVolume[Channel::right])
			{
				// set the menuState to normal
				*muteState = MuteState::normal;
			}

			// when the left channel volume is lower then the old volume
			if (volume[Channel::left] < oldVolume[Channel::left])
			{
				// increse the left channel volume by one
				volume[Channel::left]++;
			}

			// when the right channel volume is lower then the old volume
			if (volume[Channel::right] < oldVolume[Channel::right])
			{
				// increse the right channel volume by one
				volume[Channel::right]++;
			}
		}
		break;

	case MuteState::goToMinimal:
		// go to the minimal value

		// select the highest volume
		maxValue = (oldVolume[Channel::left] > oldVolume[Channel::right]) ? oldVolume[Channel::left] : oldVolume[Channel::right];


		// only activate in a interval
		// the interval is the time this fuction needs to be, devided by the maximaum value
		if (millis() - lastUpdateTime > goToMuteTime / maxValue)
		{
			lastUpdateTime = millis();

			// when the current volume is set to the minimal value
			if (volume[Channel::left] <= channelMinValue && volume[Channel::right] <= channelMinValue)
			{
				// set the muteState to minimal
				*muteState = MuteState::minimal;
			}

			// when the left channel volume is higher then the minimum value
			if (volume[Channel::left] > channelMinValue)
			{
				// decrese the left channel volume by one
				volume[Channel::left]--;
			}

			// when the right channel volume is higher then the minimum value
			if (volume[Channel::right] > channelMinValue)
			{
				// decrese the right channel volume by one
				volume[Channel::right]--;
			}
		}
		break;
	}
}