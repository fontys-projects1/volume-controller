/*
 Name:		Volume_regelaar.ino
 Created:	11-10-2022 17:23:53 AM
 Author:	Tommy de Wever
*/

#include "volumeControl.h"
#include "pushButton.h"
#include "digipot.h"

const int blinkInterval = 200;	// led blink delay in ms
const int repeatDelay = 50;		// delay in ms to repeat the action when the button is holding down

// defines the pins
enum Pinnen
{
	pin_buttonUp = 11,
	pin_buttonDown = 12,
	pin_buttonMute = 10,
	pin_ledUp = A0,
	pin_ledDown = A1,
	pin_chipSelect_left = 5,
	pin_chipSelect_right = 8,
	pin_upDown = 7,
	pin_increment = 6,
	pin_infraredRX = 9
};

// construct the buttons
pushButton buttonUp(Pinnen::pin_buttonUp);
pushButton buttonDown(Pinnen::pin_buttonDown);
pushButton buttonMute(Pinnen::pin_buttonMute);

// construct the potentiometers
digipot channelLeft(Pinnen::pin_chipSelect_left, Pinnen::pin_increment, Pinnen::pin_upDown);
digipot channelRight(Pinnen::pin_chipSelect_right, Pinnen::pin_increment, Pinnen::pin_upDown);

// this fuction will only run ones at startup
void setup()
{
	// set the led pins to output
	pinMode(Pinnen::pin_ledUp, OUTPUT);
	pinMode(Pinnen::pin_ledDown, OUTPUT);

	// init the leftchannel potentiometer
	channelLeft.goToZero();					// set to zero
	channelLeft.maxValue = channelMaxValue;	// set the max value
	channelLeft.minValue = channelMinValue;	// set the min value

	// init the rightchannel potentiometer
	channelRight.goToZero();				// set to zero
	channelRight.maxValue = channelMaxValue;// set the max value
	channelRight.minValue = channelMinValue;// set the min value
}

// the loop fuction is a while(true) loop
void loop()
{
	// creat and init the main storage variables (static)
	static uint8_t muteState = MuteState::normal;						// will store the mute state
	static uint8_t volume[2] = { channelMinValue, channelMinValue };	// will store the volume of the lef and right channel

	// create the led status booleans, can be set when needed. the leds wil be writen at the end of the loop
	bool ledUpState = false, ledDownState = false;

	/*
	* handel the actions when a button is pressed or holding down
	*/

	// read the buttons
	bool buttonUpMemory = buttonUp.read();
	bool buttonDownMemory = buttonDown.read();
	bool buttonMuteMemory = buttonMute.read();

	// when mute is pushed
	if (buttonMuteMemory)
	{
		switch (muteState)
		{
		case MuteState::normal:
		case MuteState::goToNormal:
			// set the mute state to goToMinimal
			muteState = MuteState::goToMinimal;
			break;
		case MuteState::goToMinimal:
		case MuteState::minimal:
			// set the mute state to goToNormal
			muteState = MuteState::goToNormal;
			break;
		}
	}


	// when not in a mute state, mutestate is normal or goToNormal
	if ((muteState == MuteState::normal) || (muteState == MuteState::goToNormal))
	{
		// when the butotn up is pressed   or holding down (holding down with a interval appleyd)
		// and the button down isn't pressed or holding down
		if ((buttonUpMemory || (buttonUp.isHoldingDown && buttonUp.interval.activate(repeatDelay)))
			&& !(buttonDownMemory || buttonDown.isHoldingDown))
		{
			// when the mute button is holdind down
			if (buttonMute.isHoldingDown)
			{
				// set te ballanse the die right 
				editVolumeMemory(volume, Channel::left, 1);
				editVolumeMemory(volume, Channel::right, -1);
			}
			else
			{
				// increse both channels by one
				editVolumeMemory(volume, Channel::both, 1);
			}
		}

		// when the button down is pressed or holding down (holding down with a interval appleyd)
		// and the button up isn't pressed or holding down
		if ((buttonDownMemory || (buttonDown.isHoldingDown && buttonDown.interval.activate(repeatDelay)))
			&& !(buttonUpMemory || buttonUp.isHoldingDown))
		{
			// when the mute button is holdind down
			if (buttonMute.isHoldingDown)
			{
				// set te ballanse the die left 
				editVolumeMemory(volume, Channel::left, -1);
				editVolumeMemory(volume, Channel::right, 1);
			}
			else
			{
				// decrese both channels by one
				editVolumeMemory(volume, Channel::both, -1);
			}
		}
	}

	/*
	* handel the ledstatus
	*/

	// when the left or right channel is equal to the maximum value
	if (volume[Channel::left] == channelMaxValue || volume[Channel::right] == channelMaxValue)
	{
		// turn on the up led
		ledUpState = true;
	}

	// when the left or right channel is equal to the minimum value
	if (volume[Channel::left] == channelMinValue || volume[Channel::right] == channelMinValue)
	{
		// when the mutestate is minimal
		if (muteState == MuteState::minimal)
		{
			// blink the down led
			
			// create the needed variables
			static bool blinkState;
			static uint32_t oldBlinkTime;

			// when the blink interval exeeds
			if (millis() - oldBlinkTime > blinkInterval)
			{
				// reset the time
				oldBlinkTime = millis();

				// toggle the blinkState
				blinkState = !blinkState;
			}

			// xor the up led with the blink state
			ledDownState ^= blinkState;
		}
		else
		{
			// turn on the up led
			ledDownState = true;
		}
	}

	/*	
	* handels the mute state
	*/
	
	// handels a muteState change (fading in/out)
	mute_processor(volume, &muteState);


	/*
	* Write the outputs
	*/

	// write the leds
	digitalWrite(Pinnen::pin_ledUp, ledUpState);
	digitalWrite(Pinnen::pin_ledDown, ledDownState);

	// write the potentiometers
	channelLeft.setPot(volume[Channel::left]);
	channelRight.setPot(volume[Channel::right]);
}
